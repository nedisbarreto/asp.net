﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milestone1AA
{
    class BankAccount
    {
        //what are the attributes?
        private decimal _balance;
        public const decimal depositMIN = 0;
        public const decimal depositMAX = 10000;

        public BankAccount()
        {
            _balance = 0;
        }

        public BankAccount(decimal balance_)
        {
            _balance = balance_;
        }

        //functionality:
        //withdraw and deposit (methods)
        //check balance: getter(method) or properties

        public decimal Balance{
            get
            {
                return _balance;
            }
        }

        public bool Deposit(decimal amount)
        {
            if(amount <= depositMIN || amount >= depositMAX)
            {
                throw new ArgumentOutOfRangeException("Deposit", "Deposited amount is out of range");
            }
            else
            {
                _balance += amount;
                return true;
            }
        }

        public bool Withdraw(decimal amount)
        {
            if (amount > _balance)
            {
                throw new ArgumentOutOfRangeException("Withdraw", "Withdraw amount greater than balance");
            }
            else
            {
                _balance -= amount;
                return true;
            }
        }
    }
}
