﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Employee
    {
        int idNumber;
        string name, department, position;

        public Employee()
        {
            idNumber = 0;
            name = "";
            department = "";
            position = "";
        }

        public Employee(int _idNumber, string _name, string _department, string _position)
        {
            idNumber = _idNumber;
            name = _name;
            department = _department;
            position = _position;
        }

        public Employee(int _idNumber, string _name)
        {
            idNumber = _idNumber;
            name = _name;
            department = "";
            position = "";
        }

        public int IdNumber
        {
            get
            {
                return this.idNumber;
            }
            //set
            //{
            //    this.idNumber = value;
            //}
        }


        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }

        public string Department
        {
            get
            {
                return this.department;
            }
            set
            {
                this.department = value;
            }
        }

        public string Position
        {
            get
            {
                return this.position;
            }
            set
            {
                this.position = value;
            }
        }

        public override string ToString()
        {
            return string.Format("Name: {0}, ID: {1}, Department: {2}, Position: {3}", Name, IdNumber, Department, Position);
        }

    }
}
