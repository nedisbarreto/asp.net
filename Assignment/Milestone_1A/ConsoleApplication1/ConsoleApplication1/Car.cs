﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Car
    {
        const int MINSPEED = 0;
        const int MAXSPEED = 160;
        const int BRAKING_ACCELERATION = 5;

        int yearModel, speed;
        string make;

        public Car()
        {
            speed = 0;
            yearModel = 0;
            make = "";
        }

        public Car(int _yearModel, string _make)
        {
            speed = 0;
            yearModel = _yearModel;
            make = _make;
        }

        public string Make
        {
            get
            {
                return this.Make;
            }
            set
            {
                this.Make = value;
            }
        }

        public int YearModel
        {
            get
            {
                return this.YearModel;
            }
            set
            {
                this.YearModel = value;
            }
        }

        public int Speed
        {
            get
            {
                return this.speed;
            }
        }

        public bool Accelerate()
        {
            if (speed > (MAXSPEED - BRAKING_ACCELERATION))
            {
                throw new ArgumentOutOfRangeException("MAXSPEED", "You can not pass the top speed of the car (" + MAXSPEED + "km/h)");
            }
            else
            {
                speed = speed + BRAKING_ACCELERATION;
                return true;
            }
        }

        public bool Brake()
        {
            if (speed >= (MINSPEED + BRAKING_ACCELERATION))
            {
                speed = speed - BRAKING_ACCELERATION;
                return true;
            }
            else if (speed < (MINSPEED + BRAKING_ACCELERATION))
            {
                speed = 0;
                return true;
            }
            else
            {
                throw new ArgumentOutOfRangeException("MINSPEED", "The car is already stoped");
            }
        }
    }
}
