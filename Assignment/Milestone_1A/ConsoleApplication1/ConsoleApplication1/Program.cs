﻿using ConsoleApplication1;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace milestone1AA
{
    class Program
    {
        static void Main(string[] args)
        {
            int choice;
            int min = 0;
            int max = 3;
            
            do
            {
                Console.Clear();
                Console.WriteLine("** MAIN MENU **");
                Console.WriteLine("Make your choice: ");
                Console.WriteLine("1)BankAccount");
                Console.WriteLine("2)Employee list");
                Console.WriteLine("3)Car Speed");
                Console.WriteLine("0)Quit");

                while (!int.TryParse(Console.ReadLine(), out choice) || choice < min || choice > max)
                {
                    Console.WriteLine("Error! Not valid menu choice.");
                    Console.WriteLine("Please enter again: ");
                }

                switch (choice)
                {
                    case 0:
                        break;
                    case 1:
                        Q1();
                        break;
                    case 2:
                        Q2();
                        break;
                    case 3:
                        Q3();
                        break;
                    default:
                        Console.WriteLine("An unexpected error occurred!");
                        break;
                }
            } while (choice != 0);
        }

        public static void Q1()
        {
            int choice;
            int min = 0;
            int max = 3;
            BankAccount b1 = new BankAccount();
            Console.Clear();
            do
            {
                Console.WriteLine("** BankAccount MENU **");
                Console.WriteLine("Make your choice: ");
                Console.WriteLine("1)Deposit");
                Console.WriteLine("2)Withdraw");
                Console.WriteLine("3)Balance");
                Console.WriteLine("0)Quit");

                while (!int.TryParse(Console.ReadLine(), out choice) || choice < min || choice > max)
                {
                    Console.WriteLine("Error! Not valid menu choice.");
                    Console.WriteLine("Please enter again: ");
                }

                switch (choice)
                {
                    case 0:
                        return;
                    case 1:
                        try
                        {

                            Console.WriteLine("");
                            Console.WriteLine("Deposit amount: ");
                            decimal theAmount;
                            while (!decimal.TryParse(Console.ReadLine(), out theAmount))
                            {
                                Console.WriteLine("");
                                Console.WriteLine("Invalid amount");
                                Console.WriteLine("Please enter again: ");
                            }
                            if (b1.Deposit(theAmount))
                            {
                                Console.WriteLine("");
                                Console.WriteLine("Deposit Successful!");
                            }
                            Console.WriteLine("Balance: " + b1.Balance);
                            Console.WriteLine("");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 2:
                        try
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Withdraw amount: ");
                            decimal theAmount;
                            while (!decimal.TryParse(Console.ReadLine(), out theAmount))
                            {
                                Console.WriteLine("");
                                Console.WriteLine("Invalid amount");
                                Console.WriteLine("Please enter again: ");
                            }
                            if (b1.Withdraw(theAmount))
                            {
                                Console.WriteLine("");
                                Console.WriteLine("Withdraw Successful!");
                                Console.WriteLine("");
                            }
                            Console.WriteLine("Balance: " + b1.Balance);
                            Console.WriteLine("");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 3:
                        try
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Balance: " + b1.Balance);
                            Console.WriteLine("");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    default:
                        Console.WriteLine("An unexpected error occurred!");
                        break;
                }
            } while (choice != 0);
        }

        public static void Q2()
        {
            int choice;
            int min = 0;
            int max = 0;
            Console.Clear();
            do
            {
                Employee e1 = new Employee();
                //e1.IdNumber = 47899;
                e1.Name = "Susan Meyers";
                e1.Department = "Accounting";
                e1.Position = "Vice President";

                Employee e2 = new Employee(39119, "Mark Jones", "IT", "Programmer");

                Employee e3 = new Employee(81774, "Joy Rogers");
                e3.Department = "Manufacturing";
                e3.Position = "Engineer";

                Console.WriteLine("** Employee list **");
                Console.WriteLine(e1.ToString());
                Console.WriteLine(e2.ToString());
                Console.WriteLine(e3.ToString());
                Console.WriteLine("");
                Console.WriteLine("0)Quit");

                while (!int.TryParse(Console.ReadLine(), out choice) || choice < min || choice > max)
                {
                    Console.WriteLine("Error! Not valid menu choice.");
                    Console.WriteLine("Please enter again: ");
                }

                switch (choice)
                {
                    case 0:
                        return;
                    default:
                        Console.WriteLine("An unexpected error occurred!");
                        break;
                }
            } while (choice != 0);
        }

        public static void Q3()
        {
            int choice;
            int min = 0;
            int max = 3;
            Car car = new Car();
            Console.Clear();
            do
            {
                Console.WriteLine("** Car MENU **");
                Console.WriteLine("Make your choice: ");
                Console.WriteLine("1)Accelerate");
                Console.WriteLine("2)Brake");
                Console.WriteLine("3)Check Speed");
                Console.WriteLine("0)Quit");

                while (!int.TryParse(Console.ReadLine(), out choice) || choice < min || choice > max)
                {
                    Console.WriteLine("Error! Not valid menu choice.");
                    Console.WriteLine("Please enter again: ");
                }

                switch (choice)
                {
                    case 0:
                        return;
                    case 1:
                        try
                        {

                            Console.WriteLine("");
                            Console.WriteLine("Old speed: " + car.Speed);
                            Console.WriteLine("Accelerate!");
                            car.Accelerate();
                            Console.WriteLine("New speed: " + car.Speed);
                            Console.WriteLine("");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 2:
                        try
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Old speed: " + car.Speed);
                            Console.WriteLine("Brake!");
                            car.Brake();
                            Console.WriteLine("New speed: " + car.Speed);
                            Console.WriteLine("");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case 3:
                        try
                        {
                            Console.WriteLine("");
                            Console.WriteLine("Actual speed: " + car.Speed);
                            Console.WriteLine("");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    default:
                        Console.WriteLine("An unexpected error occurred!");
                        break;
                }
            } while (choice != 0);
        }
    }
}
