﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MVCMOVIE1.Models
{
    public class MVCMOVIE1Context : DbContext
    {
        public MVCMOVIE1Context (DbContextOptions<MVCMOVIE1Context> options)
            : base(options)
        {
        }

        public DbSet<MVCMOVIE1.Models.Movie> Movie { get; set; }
    }
}
