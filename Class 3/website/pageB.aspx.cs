﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pageB : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string firstName = Request.QueryString["firstname"];
        string lastName = Request.QueryString["lastName"];

        lblFullName.Text = "Full name: " + firstName + " " + lastName;
        lblGender.Text = "Gender: " + Request.QueryString["gender"];
        lblBirthDate.Text = "Birth Date: " + Request.QueryString["birthdate"];
        lblCivilStatus.Text = "Civil Status: " + Request.QueryString["civilstatus"];
    }
}