﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class pageA : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        Response.Redirect("pageB.aspx?firstname=" + txtFirstName.Text + "&lastname=" + txtLastName.Text + "&gender=" + ddlGender.SelectedItem.Text + "&birthdate=" + calBirthDate.SelectedDate.ToString("dd/MM/yyyy") + "&civilstatus=" + rblCivilStatus.SelectedItem.Text);
    }
}