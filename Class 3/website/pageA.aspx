﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="pageA.aspx.cs" Inherits="pageA" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <p>
            <asp:Label ID="lblFirstName" runat="server" Text="First Name: "></asp:Label>
            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="lblLastName" runat="server" Text="Last Name: "></asp:Label>
            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
        </p>
        <p>
            <asp:Label ID="lblGender" runat="server" Text="Gender: "></asp:Label>
            <asp:DropDownList ID="ddlGender" runat="server">
                <asp:ListItem Enabled="true" Text="Select Gender" Value="-1"></asp:ListItem>
                <asp:ListItem Text="Male" Value="1"></asp:ListItem>
                <asp:ListItem Text="Female" Value="2"></asp:ListItem>
            </asp:DropDownList>
        </p>
        <p>
            <asp:Label ID="lblBirthDate" runat="server" Text="Birth Date: "></asp:Label>
            <asp:Calendar ID="calBirthDate" runat="server"></asp:Calendar>
        </p>
        <p>
            <asp:Label ID="lblCivilStatus" runat="server" Text="Civil Status: "></asp:Label>
            <asp:RadioButtonList ID="rblCivilStatus" runat="server">
                <asp:ListItem Text="Single" Value="1"></asp:ListItem>
                <asp:ListItem Text="Married" Value="2"></asp:ListItem>
                <asp:ListItem Text="Divorced" Value="3"></asp:ListItem>
                <asp:ListItem Text="Widowed" Value="4"></asp:ListItem>
            </asp:RadioButtonList>
        </p>
        <p>
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
        </p>
    </form>
</body>
</html>
